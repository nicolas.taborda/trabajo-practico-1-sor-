#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <ctype.h>
#define MAX_COMMAND_LENGTH 100
#define MAX_NUMBER_OF_PARAMS 10

void parseCmd(char *cmd, char **params) // FUncion que parsea el contenido de cmd y lo separa para que lo pueda leer correctamente la instruccion execvp
{
    for (int i = 0; i < MAX_NUMBER_OF_PARAMS; i++)
    {
        params[i] = strsep(&cmd, " "); // separo cada vez que encuentro un espacio y voy agregando como tokens al arreglo params

        if (params[i] == NULL) // SI encunetro un null marco el final de la linea
        {
            params[i] = '\0';
            break;
        }
    }
}

int main()
{
    char cmd[MAX_COMMAND_LENGTH];                  // variable que guarda la linea ingresada por el usuario
    char *params[MAX_NUMBER_OF_PARAMS + 1];        // arreglo con instrucciones separadas del cmd
    char comandoExit[MAX_COMMAND_LENGTH] = "exit"; // arreglo para marcar el comando de salida
    char *directorio;                              // arreglo para guardar la doreccion para el cd
    int aux;                                       // variable auxiliar para controlar el cd correcto
    char *login;                                   // variable para mostrar el user actual
    pid_t pid;                                     // pid del proceso
    int status;                                    // estado del proceso hijo

    while (1)
    {
        login = getenv("USER");
        printf("[@%s]: ", login);
        fgets(cmd, sizeof(cmd), stdin);

        parseCmd(cmd, params); // mestodo para parsear argumentos

        if (cmd[strlen(cmd) - 1] == '\n') // estp quita el salto de linea que lel agrega el fgets, sino no se puede leer por un caracter de mas en la instruccion
        {
            cmd[strlen(cmd) - 1] = '\0';
        }

        if (strncmp(cmd, comandoExit, strlen(cmd)) == 0) // si se ingresa exit sale del interprete
        {
            exit(0);
        }

        if (strcmp(params[0], "cd") == 0) // if para manejar la instruccion CD
        {
            directorio = params[1];
            directorio[strlen(directorio) - 1] = '\0';
            aux = chdir(directorio); // chdir funciona como la instruccion cat ya que execvp no la puede ejecutar

            if (aux == (-1))
                perror("Error en cd...");

            char cwd[1024]; // en char cwd se almacena la ruta actual

            if (getcwd(cwd, sizeof(cwd)) != NULL)
            {
                fprintf(stdout, "Directorio actual: %s\n", cwd);
            }
            else
            {
                perror("getcwd() error");
            }
            continue;
        }

        pid = fork(); // se crea el proceso hijo

        switch (pid)
        {
        case -1: // en el caso que no se hall podido ejecutar el fork correctamente
            perror("No se ha podido crear el proceso hijo\n");
            break;
        case 0:
            execvp(cmd, params); // execvp ejecuta las instrucciones pasadas como parametros, esta instruccion
                                 // termina con el programa principal y finaliza automaticamente luego de ejecutarse.
        default:
            waitpid(pid, &status, 0);
        }
    }
    return 0;
}