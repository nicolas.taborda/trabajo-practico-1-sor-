#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#define tamanio 100
#define tamanioParametros 10

void limpiar(char *cadena) // quita el enter de la linea ingresada por el usuario
{
    char *caracter;
    caracter = strchr(cadena, '\n');
    if (caracter)
        *caracter = '\0';
}

void separar(char *cadena, char **parametros) // separa la cedena introducida por el usuario en parametros
{
    char delimitador[] = " \n\t";
    char *token = strtok(cadena, delimitador);
    int i = 0;
    parametros[i] = token;
    i++;
    if (token != NULL)
    {
        while (token != NULL)
        {
            token = strtok(NULL, delimitador);
            parametros[i] = token;
            i++;
        }
    }
}

int main()
{
    char cadena[tamanio];
    char *parametros[tamanioParametros + 1];
    __pid_t pid;
    int estado;
    char *directorio; // arreglo para guardar la direccion para el cd
    int aux;   
    char *login;

    while (1)
    {
         login = getenv("USER");
        printf("[@%s]: ", login);
        
        fgets(cadena, tamanio, stdin);

        limpiar(cadena);

        char *punteroCadena = cadena;

        separar(punteroCadena, parametros);

        if(strlen(cadena)==0)
        {   
            printf("Para salir ingrese exit \n");
            continue;
        }
            

        if (strncmp(cadena, "exit", strlen(cadena)) == 0) // si se ingresa exit sale del interprete
            exit(0);


        if (strcmp(parametros[0], "cd") == 0) // if para manejar la instruccion CD
        {
            directorio = parametros[1];
            aux = chdir(directorio); // chdir funciona como la instruccion cat ya que execvp no la puede ejecutar

            if (aux == (-1))
                perror("Error en cd...");

            char cwd[1024]; // en char cwd se almacena la ruta actual

            if (getcwd(cwd, sizeof(cwd)) != NULL)
            {
                fprintf(stdout, "Directorio actual: %s\n", cwd);
            }
            else
            {
                perror("getcwd() error");
            }
            continue;
        }

        pid = fork();

        switch (pid)
        {
        case -1:
            perror("error \n");
            break;
        case 0:
            execvp(cadena, parametros);
        default:
            waitpid(pid, &estado, 0);
        }
    }
    return 0;
}